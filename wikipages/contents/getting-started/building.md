---
title: Building
sidebar: home_sidebar
keywords: build, building
type: homepage
toc: false
permalink: /contents/getting-started/building.html
---

{{site.data.alerts.note}} This page has been moved. {{site.data.alerts.end}}

This page has been moved to the readme in the [main orbit repository](https://git.erlendjs.no/orbit/orbit).

## How to build and flash using CLion ##

{{site.data.alerts.note}} This section is outdated. Use at own risk. CLion is not necessary for programming, this is only for the interested. {{site.data.alerts.end}}

CLion requires a bit of extra work to get going. First, point it to the `CMakeLists.txt` file in the root directory to open the project. The navigate to `File->Settings->Build,Execution,Deplyment->CMake` and make shure the Generation path is set to `.idea/cmake-build`. This prevents any garbage to be included by git later on.

Next, navigate to `File->Settings->Editor->Code Style` and set the Scheme at the top to Project. The repository includes some settings that set the code style appropriately, which will help writing code that is consistent with the rest of the project.

We now need to set build configuration appropriately. Click on the small drop-down in the upper right corner and select "Edit Configurations...". Create a configuration matching the one in the image below.

<figure style="float:center; width: 100%;">
	<a href="/images/getting-started/run-debug-configurations.png">
	<img style="width: 100%;" title="Overview" src="{{ "/images/getting-started/run-debug-configurations.png" | prepend: site.baseurl }}" />
	</a>
	<figcaption>Appropriate Configuration.</figcaption>
</figure>

Make shure that the target is set to `nuts_obc_os`, and set the executable to your make program (`/usr/bin/make` on Linux). The argument should be `flash` and the working directory must point to our project root.

This configuration tricks CLion into two things: First off it runs the proper Makefile in our root directory instead of using CMake when we build. Second, when we use the "Run" button in the IDE it invokes `make flash` after successfully building the source, flashing the binary to the microcontroller. Note that this currently only works with the Segger, if you are using UniFlash you still need to use that.

In order to build you may click the build button to the left of the top-right drop-down. Upload code to the microcontroller using the run button.

## Useful links ##

 * [CMake profile](https://www.jetbrains.com/help/clion/configuring-cmake.html)
 * [Nice summary page](http://rtime.felk.cvut.cz/hw/index.php/TMS570LS3137#GCC_build_for_Cortex-R4_Big-Endian)
 * [TI Forum: TMS570LS and GCC compiler](https://e2e.ti.com/support/microcontrollers/hercules/f/312/t/413871)
 * [GNU ARM Embedded Toolchain: 4.9 2015q1 BigEndian patch](https://answers.launchpad.net/gcc-arm-embedded/+question/263922)
 * [GNU ARM Embedded Toolchain: Support for TMS570 Cortex-R4F Big-Endian](https://answers.launchpad.net/gcc-arm-embedded/+question/189066)
 * [TI Forum (maybe useful in the future): LAUNCHXL2-TMS57012: Unable to perform a cold reset when flashing code using XDS debugger from a GCC toolchain](https://e2e.ti.com/support/microcontrollers/hercules/f/312/p/589209/2164411#pi320098=2)
