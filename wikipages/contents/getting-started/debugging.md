---
title: Debugging
sidebar: home_sidebar
keywords: debug, debugging
type: homepage
toc: false
permalink: /contents/getting-started/debugging.html
---

## Introduction ##

There are two ways to debug: KDBG, which is a light-weight graphical front-end for GDB, and using CLion. Using the GNU debugger directly should also work. Communicating with the microcontroller for user input and output is done through the serial interfaces.

## Seeing output from the controller ##

The TMS570 contains two serial interfaces that may be used to display messages and take input. The first is connected to the debug port on left side on the board. The second is the three-pin connector to the right of the processor. You will need to use the second one for debug I/O due to a DMA conflict. The pins are as follows, from the top: TX, RX, GND. Connect a USB to serial adapter here. On Linux you should see it as `/dev/ttyUSB0`. On Windows, you have to download PuTTY or TeraTerm and connect to the correct port with a baud of 38400. Check the settings so that the newline character (`\n`) is properly interpreted as a newline.

On Linux, minicom and screen are popular options. In any case make sure you can access serial ports without sudo. If you find that you need sudo to open serial interfaces, use `sudo adduser myusername dialout` to add yourself to the dialout group. If you want to use minicom, install it using `sudo apt install minicom`. It is highly recommended that you enable colors. Do this by adding `export MINICOM="-c on"` to `~/.bashrc`. Next, start minicom with the name of the serial interface: `minicom -D /dev/ttyACM0`. Press `Ctrl^A + Z` then `O` to open the settings. Under "Serial port setup", make sure you set the correct device and speed. Under "Screen and keyboard", make sure "Add carriage return" is set to "Yes". Then save the settings using "Save setup as dfl". You should now be able to start minicom on the command line without any arguments and be ready to go. Use `Ctrl^A` and `X` to exit minicom.

<figure style="float:center; width: 100%;">
	<a href="/images/getting-started/minicom-hello-world.png">
	<img style="width: 578px;" title="Overview" src="{{ "/images/getting-started/minicom-hello-world.png" | prepend: site.baseurl }}" />
	</a>
	<figcaption>Hello World output using Minicom.</figcaption>
</figure>

## Debugging with KDBG ##

Install the program using `apt install kdbg`. There is a target in the Makefile providing a quick shortcut to start the program, just run `make kdbg`. The first time you run kdbg, open `Settings->Global options` and make sure "How to invoke GDB" reads `arm-none-eabi-gdb --fullname --nx` then restart it. You may also want to open the toolbar settings and add "break" to the visible toolbar.

<figure style="float:center; width: 100%;">
	<a href="/images/getting-started/debugging-with-kdbg.png">
	<img style="width: 100%;" title="Overview" src="{{ "/images/getting-started/debugging-with-kdbg.png" | prepend: site.baseurl }}" />
	</a>
	<figcaption>Debugging the CAN driver using KDBG.</figcaption>
</figure>

## Debugging with CLion ##

In order to debug with CLion you need to create a configuration using the "Edit Configurations..." drop-down again. Create a "GDB Remote Debug" configuration as shown below:

<figure style="float:center; width: 100%;">
	<a href="/images/getting-started/gdb-remote-debug.png">
	<img style="width: 100%;" title="Overview" src="{{ "/images/getting-started/gdb-remote-debug.png" | prepend: site.baseurl }}" />
	</a>
	<figcaption>Appropriate Configuration.</figcaption>
</figure>

Set the GDB path to your debugger executable. The path shown is the default on most Linux installations. The target remote specifies the network address of the GDB server. These values come from the Makefile. At last you need to set the correct path to the symbol file. This file is actually the executable compiled, from before the build process creates a binary dump of it. You can find it in `./build/program.elf`.

When you are ready to debug, select the debug configuration from the drop down. Then, start the GDB server. If you use the Segger, execute `make server` from a shell in the project root. If you use the XDS110 run `make tiserver` instead, or `make tiserver-win` if you use Windows. You can do this from the terminal inside CLion if you want to skip opening a separate shell for it. At last click the debug button next to the top-right drop-down. Note that you have to switch back to the build configuration in order to build and upload code.

{{site.data.alerts.note}} As always, things are super slow and bugged on Windows. You have to build using make on the Git Bash command line, and start the debug server from there too. {{site.data.alerts.end}}

<figure style="float:center; width: 100%;">
	<a href="/images/getting-started/debugging-with-clion.png">
	<img style="width: 100%;" title="Overview" src="{{ "/images/getting-started/debugging-with-clion.png" | prepend: site.baseurl }}" />
	</a>
	<figcaption>Debugging the CAN driver using CLion.</figcaption>
</figure>

## Useful Links ##

 * [Remote GDB debug](https://www.jetbrains.com/help/clion/remote-debug.html)
 * General GDB remote usage, not directly MCU related: [davis.lbl.gov/Manuals/GDB/gdb\_17.html](http://davis.lbl.gov/Manuals/GDB/gdb_17.html)
