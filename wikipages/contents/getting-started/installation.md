---
title: Installation
sidebar: home_sidebar
keywords: installing, installation
type: homepage
toc: true
permalink: /contents/getting-started/installation.html
---

## Introduction ##

We use plain GCC and makefiles for our project. This is because it offers great flexibility, control, a small installation and at the very least it allows us to edit using our preferred text editor. With GDB we have plenty of debugging tools to chose from. IDE alternatives are discussed below and in the other getting started articles.

In order to build the project you need to install the toolchain and clone the project git-repository. Follow the instructions below to install all the necessary tools for your operating system, and consult the build page for building and installing on the microcontroller.

In order to install or work on the operating system you will need a Segger J-Link (highly recommended) or any suitable JTAG probe. For normal program development, a USB to serial adapter is all you need for now.

## Setting up your GitLab user and SSH ##

Before continuing, make certain that you have a user on the [GitLab server](https://git.erlendjs.no). Use `ssh-keygen` to create a key on your machine and add it to the user settings at GitLab. This makes it much easier to use git later on.

## Installing the toolchain on Linux ##

The following commands can be used to install the necessary tools on a fresh Linux-machine. It installs Git, Make, the compiler toolchain and the C libraries. This assumes that you are on the Xenial release, as it includes big-endian support files. If you use a different release, make sure you have the big-endian files after installing gcc and newlib. The simplest way to know is if you get a related error message when building the kernel. If you dont have the files, you need the old [big-endian support files](https://git.erlendjs.no/nuts/big-endian). Apply those after installing the rest.

```bash
sudo apt install git make gcc-arm-none-eabi libnewlib-arm-none-eabi libstdc++-arm-none-eabi-newlib gdb-arm-none-eabi
```

Remember to create an SSH key for use with GitLab and add it to your user profile at the GitLab website. You may now clone the repositories to your machine. See the instructions in the repository itself: [main orbit repository](https://git.erlendjs.no/orbit/orbit).

## Installing the toolchain on Windows ##

{{site.data.alerts.note}} Note that using Windows for this kind of embedded development is a lot messier than using Linux. If you are going to do some serious development, using Linux is highly recommended. It is much friendlier and installation is easier. Compiling on windows is also much slower than on Linux, as is debugging. {{site.data.alerts.end}}

 1.  Install Git for Windows from [here](https://git-scm.com/download/win). Once installed, open Git GUI and use it to create an SSH key. Add this key in your GitLab user profile. Doing this allows you to push and pull with ease using SSH. Note that the password is independent of the one you use at GitLab.

 2.  Install MinGW from [here](https://sourceforge.net/projects/mingw/files/). Make sure you install it in a directory with **no spaces** in the path. Once installed, open the MinGW Installation Manager. Select and install `mingw-developer-toolkit` and `msys-base`. This will install a working make tool on your computer. You do not need to install any compiler from MinGW since we need the ARM one. However, if you are going to use CLion as an IDE, you will need `mingw32-base` and `mingw32-gcc-g++` to keep it happy.

 3.  Install the ARM compiler toolchain from [here](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads). Make shure to select the one that ends in `sha2` as it is signed for Windows 7 and later. When installing, make shure you select a path with **no spaces** in it and check "Add path to environment variable" before finishing. The newest version comes with a bugged GDB, so it has to be replaced with an older one from [here](https://launchpad.net/gcc-arm-embedded/+download). Download the Windows zip-file and copy over `arm-none-eabi-gdb` in the `bin` directory.

 4.  Navigate to your compiler install directory and find `arm-none-eabi\include\sys\types.h`. At the very bottom, comment out `#include <sys/_pthreadtypes.h>`. This is a feature of the newer toolchain that conflicts with our OS for the moment.

 5.  Install the necessary C libraries for our big-endian processor. Because the newest official packages lack this for some reason, a repository has been provided at [GitLab](https://git.erlendjs.no/nuts/big-endian). Clone or download this to your computer. To install, copy the `armv7-ar` folder to your toolchains `lib/gcc/arm-none-eabi/7.2.1/thumb` folder. The version number in the path may be different on your system.

 6.  Clone the repositories from GitLab that you will use. See the instructions in the repository itself: [main orbit repository](https://git.erlendjs.no/orbit/orbit).

## Installing flash utilities for OS development ##

In order to get the compiled binary onto the microcontroller, you will have to install the appropriate tools for the debug probe you are using.

### Segger J-Link standalone probe ###

The Segger debug probe is a probe that allows easy flashing and debugging from a command line or the shell. Download the J-Link tools from the Segger website, and add it to the environment path variable. Using the deb-package option is the quickest. When you are done, you should be able to start `JLinkExe` and `JLinkGDBServer` from a terminal.

## Installing the JetBrains CLion IDE ##

Since we are part of an educational institution we have free access to the CLion IDE from JetBrains. If you prefer to code with an IDE you should consider using this, however you do not have to (any text editor and debugger with GDB-support should work). You need to register on their site using your NTNU credentials and apply for an educational licence, which will give you access to all their products for free. Install CLion as normal and apply the licence.

{{site.data.alerts.note}} Starting the build process from within CLion does not work on Windows. Open Git Bash and run make in there instead. {{site.data.alerts.end}}

{{site.data.alerts.note}} Debugging with CLion is very slow on Windows, and will not work if you forgot to patch the toolchain with an older GDB. {{site.data.alerts.end}}
